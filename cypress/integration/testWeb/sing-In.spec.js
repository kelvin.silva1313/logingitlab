/// <reference types="cypress" />

describe("example to-do app", () => {
  beforeEach(() => {
    cy.visit("https://about.gitlab.com/");
  });

  it("Login Gitlab", () => {
    // We use the `cy.get()` command to get all elements that match the selector.
    // Then, we use `should` to assert that there are two matched items,
    // which are the two default items.
    cy.get("#slpMobileNavActive > .slp-inline-block").click();
    cy.get(
      '.navigation-mobile__dropdown-active > [href="https://gitlab.com/users/sign_in"]'
    ).click();
    cy.wait(7000);
    cy.get('[data-qa-selector="login_field"]').type(Cypress.env("EMAIL"));
    cy.get('[data-qa-selector="password_field"]').type(Cypress.env("PASSWORD"));
    cy.get('[data-qa-selector="sign_in_button"]').click();
    cy.contains("Welcome to GitLab");
  });
});
